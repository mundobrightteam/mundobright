package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mercadopago.MercadoPago;
import com.mercadopago.resources.Preference;
import com.mercadopago.resources.datastructures.preference.Address;
import com.mercadopago.resources.datastructures.preference.BackUrls;
import com.mercadopago.resources.datastructures.preference.Identification;
import com.mercadopago.resources.datastructures.preference.Item;
import com.mercadopago.resources.datastructures.preference.Payer;
import com.mercadopago.resources.datastructures.preference.Phone;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.template.velocity.VelocityTemplateEngine;

public class ControladorVenta {
    public static Route getVentas = (Request request, Response response) -> {
            
        MercadoPago.SDK.setAccessToken("TEST-6004468078258254-100814-9c68fc1b2328af18ac88457cbca9cf5c-190221195");
    
        VentaDAO ventaDAO= new VentaDAO();
        List<Venta> ventas = new ArrayList<>();
        ventas = ventaDAO.getAll();
        for (Venta v : ventas) {
            JSONObject preference = new JSONObject(MercadoPago.SDK.Get("https://api.mercadopago.com/checkout/preferences/"+v.getId_preferencia()+"?access_token="+MercadoPago.SDK.getAccessToken().toString()).getStringResponse());
            JSONObject persona = preference.getJSONObject("payer");
            JSONObject direccion = persona.getJSONObject("address");
            v.setPago(preference.getBoolean(("expires")));
            v.setCalle(direccion.getString("street_name"));
            v.setNro_calle(direccion.getInt("street_number"));
            v.setCp(direccion.getString("zip_code"));
            v.setTel(persona.getJSONObject("phone").getInt("number"));
            System.out.println(preference);
            v.setNombre(persona.getString("name")+" "+persona.getString("surname"));
            JSONArray items = preference.getJSONArray("items");
            double total = 0;
            for (int i = 0; i < items.length(); i++) {
                JSONObject obj = items.getJSONObject(i);
                total = total + (obj.getInt("quantity")*obj.getDouble(("unit_price")));
            }
            v.setTotal((double)Math.round(total * 100d)/100d);
        }
        ObjectMapper mapperObj = new​ ObjectMapper();
        String​ jsonStr​ = mapperObj.writeValueAsString(ventas);
        System.out.println(jsonStr);
        return​ jsonStr;
    };
    
    public static Route getVenta = (Request request, Response response) -> {
        int id_venta = Integer.parseInt(request.queryParams("id_venta"));

        MercadoPago.SDK.setAccessToken("TEST-6004468078258254-100814-9c68fc1b2328af18ac88457cbca9cf5c-190221195");
    
        VentaDAO ventaDAO= new VentaDAO();
        Venta venta = ventaDAO.getVenta(id_venta);
        JSONObject preference = new JSONObject();
        if (venta != null){
            preference = new JSONObject(MercadoPago.SDK.Get("https://api.mercadopago.com/checkout/preferences/"+venta.getId_preferencia()+"?access_token="+MercadoPago.SDK.getAccessToken().toString()).getStringResponse());
        }
        return​ preference;
    };
}
