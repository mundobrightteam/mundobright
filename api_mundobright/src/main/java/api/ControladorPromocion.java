package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONObject;
import spark.Request;
import spark.Response;
import spark.Route;

public class ControladorPromocion {
    public static Route setPromocion= (Request req, Response res)-> {
        JSONObject resquest_body = new JSONObject(req.body());

        int id_producto = resquest_body.getInt("id_producto");
        double precio_promocion = resquest_body.getDouble("precio_promocion");
        Date f1 = new SimpleDateFormat("yyyy-MM-dd").parse(resquest_body.getString("f1").substring(0, 10));
        Date f2 = new SimpleDateFormat("yyyy-MM-dd").parse(resquest_body.getString("f2").substring(0, 10));
        PromocionDAO promocionDAO= new PromocionDAO();
        boolean add = promocionDAO.setPromocion(id_producto, precio_promocion, f1, f2);
        Promocion p = new Promocion();
        if(add){
            p = promocionDAO.getPromocion(id_producto);
        }else{
            p = null;
        }
        ObjectMapper mapperObj = new​ ObjectMapper();
        String​ jsonStr​ = mapperObj.writeValueAsString(p);
        System.out.println(jsonStr);
        return​ jsonStr;
    };
    
    public static Route updatePromocion= (Request req, Response res)-> {
        JSONObject resquest_body = new JSONObject(req.body());

        int id_producto = resquest_body.getInt("id_producto");
        double precio_promocion = resquest_body.getDouble("precio_promocion");
        Date f1 = new SimpleDateFormat("yyyy-MM-dd").parse(resquest_body.getString("f1").substring(0, 10));
        Date f2 = new SimpleDateFormat("yyyy-MM-dd").parse(resquest_body.getString("f2").substring(0, 10));
        PromocionDAO promocionDAO= new PromocionDAO();
        
        boolean value = promocionDAO.updatePromocion(id_producto, precio_promocion, f1 ,f2);
        ObjectMapper mapperObj = new​ ObjectMapper();
        String​ jsonStr​ = mapperObj.writeValueAsString(value);
        System.out.println(jsonStr);
        return​ jsonStr;
    };
    
    public static Route deletePromocion= (Request req, Response res)-> {
        JSONObject resquest_body = new JSONObject(req.body());

        int id_promocion = resquest_body.getInt("id_promocion");

        PromocionDAO promocionDAO= new PromocionDAO();
        boolean value = promocionDAO.deletePromocion(id_promocion);
        
        ObjectMapper mapperObj = new​ ObjectMapper();
        String​ jsonStr​ = mapperObj.writeValueAsString(value);
        System.out.println(jsonStr);
        return​ jsonStr;
    };
}