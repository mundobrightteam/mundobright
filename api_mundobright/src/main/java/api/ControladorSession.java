package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mercadopago.MercadoPago;
import com.mercadopago.resources.Preference;
import com.mercadopago.resources.datastructures.preference.Address;
import com.mercadopago.resources.datastructures.preference.BackUrls;
import com.mercadopago.resources.datastructures.preference.Identification;
import com.mercadopago.resources.datastructures.preference.Item;
import com.mercadopago.resources.datastructures.preference.Payer;
import com.mercadopago.resources.datastructures.preference.Phone;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.template.velocity.VelocityTemplateEngine;

public class ControladorSession {
    public static Route autenticar = (Request request, Response response) -> {
        JSONObject respuesta = new JSONObject();
        request.session(true);
        if(request.session().attribute("id")!=null){
            respuesta.accumulate("value", true);
            AdministradorDAO adminDAO = new AdministradorDAO();
            Administrador admin = adminDAO.getAdminByID(request.session().attribute("id"));
            respuesta.accumulate("nombre", admin.getNombre());
        }else{
            respuesta.accumulate("value", false);
        }
        return respuesta;
    };
    
    public static Route login = (Request request, Response response) -> {
        JSONObject resquest_body = new JSONObject(request.body());

        String correo = resquest_body.getString("correo");
        String password = resquest_body.getString("password");
        
        JSONObject respuesta = new JSONObject();
        if(request.session().attribute("id")==null){
            AdministradorDAO adminDAO = new AdministradorDAO();
            Administrador admin = adminDAO.getAdmin(correo, password);
            if(admin!= null){
                request.session(true);
                System.out.println(admin.getId_administrador());
                request.session().attribute("id",admin.getId_administrador());
                respuesta.accumulate("value", true);
                respuesta.accumulate("nombre", admin.getNombre());
                respuesta.accumulate("mensaje", "Session Iniciada correctamente");
            }else{
                respuesta.accumulate("value", false);
                respuesta.accumulate("mensaje", "Email o contrasenia incorrecto");
            }
        }else{
            respuesta.accumulate("value", false);
            respuesta.accumulate("mensaje", "Usted no puede iniciar session, ya ah iniciado una.");
        }
        return respuesta;
    };
    
    public static Route cerrarSession = (Request request, Response response) -> {
        JSONObject respuesta = new JSONObject();
        if(request.session().attribute("id")!=null){
            request.session().removeAttribute("id");
            respuesta.accumulate("value", true);
            respuesta.accumulate("mensaje", "Session cerrada correctamente");
        }else{
            respuesta.accumulate("value", false);
            respuesta.accumulate("mensaje", "Usted no puede iniciar session, ya que no ah iniciado una.");
        }
        return respuesta;
    };
}
