package api;

import java.util.*;
import lombok.Data;

@Data
public class Producto {
    private Double precio;
    private int cantidad;
    private String marca;
    private String nombre;
    private String imagen;
    private int id_sub_categoria;
    private int id_producto;
    private Date fecha_publicacion;
    private Promocion promocion;
    
    public Producto(){}
    
    public Producto(int cantidad,String titulo,Double precio){
        this.cantidad = cantidad;
        this.nombre = titulo;
        this.precio = precio;
    }
    
    public void setPromocion(Promocion p){
        this.promocion = p;
    }
}
