package api;

import java.util.List;
import org.sql2o.Connection;
import util.Sql2oDAO;

public class CategoriaDAO {
    public List<Categoria> getAll() {
            String query = "SELECT * FROM categorias";
        List<Categoria> res = null;
        try (Connection con = Sql2oDAO.getSql2o().open()) {
           res = con.createQuery(query).executeAndFetch(Categoria.class);
           return res;
        } catch(Exception e){
            System.out.println(e);
        }
        return null;
    } 
}
