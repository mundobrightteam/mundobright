package api;

import java.util.*;
import lombok.Data;
import org.json.JSONObject;

@Data
public class Administrador {
    private int id_administrador;
    private String nombre;
    private String email;
    private String password;
    
    public Administrador(){}
}