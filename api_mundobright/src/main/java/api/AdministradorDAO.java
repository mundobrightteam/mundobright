package api;

import java.util.List;
import org.sql2o.Connection;
import util.Sql2oDAO;

public class AdministradorDAO {
    public Administrador getAdmin(String email,String password) {
        String query = "SELECT * FROM administradores WHERE email= :email AND password = :password";
        List<Administrador> res = null;
        try (Connection con = Sql2oDAO.getSql2o().open()) {
           res = con.createQuery(query)
                   .addParameter("email", email)
                   .addParameter("password", password)
                   .executeAndFetch(Administrador.class);
           if (res.isEmpty()){
               return null;
           }
           return res.get(0);
        } catch(Exception e){
            System.out.println(e);
        }
        return null;
    }
    
    public Administrador getAdminByID(int id_administrador) {
        String query = "SELECT * FROM administradores WHERE id_administrador= :id_administrador";
        List<Administrador> res = null;
        try (Connection con = Sql2oDAO.getSql2o().open()) {
           res = con.createQuery(query)
                   .addParameter("id_administrador", id_administrador)
                   .executeAndFetch(Administrador.class);
           if (res.isEmpty()){
               return null;
           }
           return res.get(0);
        } catch(Exception e){
            System.out.println(e);
        }
        return null;
    }
}