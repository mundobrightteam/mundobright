package api;

import java.util.*;
import lombok.Data;

@Data
public class Categoria {
    private int id_categoria;
    private String nombre;
}