package api;

import java.util.List;
import org.sql2o.Connection;
import util.Sql2oDAO;

public class VentaDAO {
    public List<Venta> getAll() {
        String query = "SELECT * FROM ventas";
        List<Venta> res = null;
        try (Connection con = Sql2oDAO.getSql2o().open()) {
           res = con.createQuery(query).executeAndFetch(Venta.class);
           return res;
        } catch(Exception e){
            System.out.println(e);
        }
        return null;
    }
    
    public Venta getVenta(int id_venta) {
        String query = "SELECT * FROM ventas WHERE id_venta=:id_venta";
        List<Venta> res = null;
        try (Connection con = Sql2oDAO.getSql2o().open()) {
           res = con.createQuery(query)
                   .addParameter("id_venta", id_venta)
                   .executeAndFetch(Venta.class);
           return res.get(0);
        } catch(Exception e){
            System.out.println(e);
        }
        return null;
    }
}