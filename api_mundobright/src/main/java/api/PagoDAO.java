package api;

import java.util.List;
import org.sql2o.Connection;
import util.Sql2oDAO;

public class PagoDAO {
    
    public Integer insertVenta(String id_preferencia,String metodo_retiro) {
        String query = "INSERT INTO ventas (id_preferencia,metodo_retiro) VALUES (:id_preferencia,:metodo_retiro)";
        System.out.println(query);
        //try (Connection con = Conexion.getConexion()) {
        try (Connection con = Sql2oDAO.getSql2o().beginTransaction()) {
            con.createQuery(query).addParameter("id_preferencia", id_preferencia).addParameter("metodo_retiro", metodo_retiro).executeUpdate();
            query = "SELECT MAX(id_venta) FROM ventas";
            System.out.println(query);
            List<Integer> resp = con.createQuery(query).executeAndFetch(Integer.class);
            con.commit();
            return resp.get(0);
        } catch(Exception e){
            System.out.println(e);
        }
        
        return null;
    }
    
    public boolean insertProductos(List<ProductoVenta> productos) {
        String query = "INSERT INTO productos_x_ventas (id_venta,id_producto,cantidad,precio_unitario) VALUES (:id_venta,:id_producto,:cantidad,:precio_unitario)";
        System.out.println(query);
        //try (Connection con = Conexion.getConexion()) {
        try (Connection con = Sql2oDAO.getSql2o().beginTransaction()) {
            for(ProductoVenta producto : productos){
                con.createQuery(query).bind(producto).executeUpdate();
            }
            con.commit();
            return true;
        } catch(Exception e){
            //
        }
        
        return false;
    }
    
    public boolean preferenceIsNULL(String id_preferencia) {
        String query = "SELECT id_venta FROM ventas WHERE id_preferencia = :id_preferencia";
        System.out.println(query);
        //try (Connection con = Conexion.getConexion()) {
        try (Connection con = Sql2oDAO.getSql2o().open()) {
            List<Integer> resp = con.createQuery(query).addParameter("id_preferencia", id_preferencia).executeAndFetch(Integer.class);
            if(resp.isEmpty()){
                return false;
            }else{
                return true;
            }
        } catch(Exception e){
            System.out.println(e);
        }
        
        return false;
    }
    
}
