package api;

import java.util.List;
import org.sql2o.Connection;
import util.Sql2oDAO;

public class ProductoDAO {
    public List<Producto> getAll() {
        String query = "SELECT * FROM productos";
        List<Producto> res = null;
        try (Connection con = Sql2oDAO.getSql2o().open()) {
           res = con.createQuery(query).executeAndFetch(Producto.class);
           return res;
        } catch(Exception e){
            System.out.println(e);
        }
        return null;
    }
}
