package api;

import java.util.*;
import lombok.Data;
import org.json.JSONObject;

@Data
public class Venta {
    private int id_venta;
    private String id_preferencia;
    private String metodo_retiro;
    private Date fecha;
    private String nombre;
    private boolean pago;
    private String calle;
    private int nro_calle;
    private String cp;
    private int tel;
    private double total;

    public Venta(){}
}