package api;

import java.util.*;
import lombok.Data;

@Data
public class Promocion {
    private int id_promocion;
    private Double precio_promocion;
    private Date f1;
    private Date f2;
    
    public Promocion(){}
}