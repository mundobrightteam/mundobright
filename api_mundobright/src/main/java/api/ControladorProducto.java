package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import spark.Request;
import spark.Response;
import spark.Route;

public class ControladorProducto {
    public static Route getProductos= (Request req, Response res)-> {
        ProductoDAO productoDAO= new ProductoDAO();
        PromocionDAO promocionDAO= new PromocionDAO();
        List<Producto> productos = new ArrayList<>();
        productos = productoDAO.getAll();
        for (Producto p : productos) {
            p.setPromocion(promocionDAO.getPromocion(p.getId_producto()));
        }
        ObjectMapper mapperObj = new​ ObjectMapper();
        String​ jsonStr​ = mapperObj.writeValueAsString(productos);
        System.out.println(jsonStr);
        return​ jsonStr;
    };

    public static Route getPromociones= (Request req, Response res)-> {
        ProductoDAO productoDAO= new ProductoDAO();
        PromocionDAO promocionDAO= new PromocionDAO();
        List<Producto> productos = new ArrayList<>();
        productos = productoDAO.getAll();
        for (Producto p : productos) {
            p.setPromocion(promocionDAO.getPromocionAdmin(p.getId_producto()));
        }
        ObjectMapper mapperObj = new​ ObjectMapper();
        String​ jsonStr​ = mapperObj.writeValueAsString(productos);
        System.out.println(jsonStr);
        return​ jsonStr;
    };
}
