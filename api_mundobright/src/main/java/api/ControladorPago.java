package api;

import com.mercadopago.MercadoPago;
import com.mercadopago.resources.Preference;
import com.mercadopago.resources.datastructures.preference.Address;
import com.mercadopago.resources.datastructures.preference.BackUrls;
import com.mercadopago.resources.datastructures.preference.Identification;
import com.mercadopago.resources.datastructures.preference.Item;
import com.mercadopago.resources.datastructures.preference.Payer;
import com.mercadopago.resources.datastructures.preference.Phone;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import spark.Request;
import spark.Response;
import spark.Route;

import org.json.JSONArray;
import org.json.JSONObject;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;

public class ControladorPago {
    
    public static Route getPreferencia = (Request request, Response response) -> {
        JSONObject resquest_body = new JSONObject(request.body());

        String nombre = resquest_body.getJSONObject("info_payer").getString("nombre");
        String apellido = resquest_body.getJSONObject("info_payer").getString("apellido");
        String email = resquest_body.getJSONObject("info_payer").getString("email");
        String dni = resquest_body.getJSONObject("info_payer").getString("dni");
        String telefono = resquest_body.getJSONObject("info_payer").getString("telefono");
        String retiro = resquest_body.getJSONObject("info_payer").getString("retiro");
        String numero = resquest_body.getJSONObject("info_payer").getString("numero");
        String direccion = resquest_body.getJSONObject("info_payer").getString("direccion");
        String ciudad = resquest_body.getJSONObject("info_payer").getString("ciudad");
        String codigo_postal = resquest_body.getJSONObject("info_payer").getString("codigo_postal");
        String respuesta_url = resquest_body.getJSONObject("info_payer").getString("respuesta_url");
        
        //Agrega credenciales
        MercadoPago.SDK.setAccessToken("TEST-6004468078258254-100814-9c68fc1b2328af18ac88457cbca9cf5c-190221195");
        
        // Crea un objeto de preferencia
        Preference preference = new Preference();
        
        //Backs URls de respuesta
        //preference.setAutoReturn(Preference.AutoReturn.all);
        preference.setAutoReturn(Preference.AutoReturn.approved);
        BackUrls backUrls = new BackUrls(
            respuesta_url+"/backurl/success",
            respuesta_url+"/backurl/pending",
            respuesta_url+"/backurl/failure");

        preference.setBackUrls(backUrls);
        
        //Creo el payer
        Payer payer = new Payer();
        payer.setName(nombre)
            .setSurname(apellido)
            .setEmail(email)
            .setDateCreated("2018-06-02T12:58:41.425-04:00")
            .setPhone(new Phone()
            .setAreaCode("")
            .setNumber(dni))
            .setIdentification(new Identification()
            .setType("DNI")
            .setNumber(dni))
            .setAddress(new Address()
                .setStreetName(direccion)
                .setStreetNumber(Integer.valueOf(numero))
                .setZipCode(codigo_postal));
        
        //Administro los productos
        JSONArray items = resquest_body.getJSONArray("items");
        
        List<ProductoVenta> productos = new ArrayList<>();
        float iva = 0;
        for (int i = 0; i < items.length(); i++) {
            JSONObject obj = items.getJSONObject(i);
            
            // Agrego el ítem de la preferencia
            productos.add(new ProductoVenta(null,obj.getInt("id_producto"),obj.getInt("cantidad"),(float) obj.getFloat("precio")));
            
            
            
            Item item = new Item();
            item.setTitle(StringUtils.stripAccents(obj.getString("nombre")))
                .setQuantity(obj.getInt("cantidad"))
                .setUnitPrice((float) obj.getFloat("precio"));
            preference.appendItem(item);
            iva = iva + (((float) obj.getFloat("precio"))*((float)0.21));
        }
        //Trunco el iva a .00 debido a que mercado pago lo pide asi-
        iva=Float.parseFloat((String.format("%.2f",(double) iva)).replace(',', '.'));
        
        System.out.println(iva);
        
        //Agrego el IVA Posicion
        Item item = new Item();
        item.setTitle("IVA Posicion")
            .setQuantity(1)
            .setUnitPrice(iva);
        preference.appendItem(item);
        
        //Seteo payer y guardo preferencia
        preference.setPayer(payer);
        preference.save();
        
        PagoDAO pDAO = new PagoDAO();
        
        //Guardo la venta
        Integer id_venta = pDAO.insertVenta((String)preference.getId(),retiro);
        System.out.println(id_venta);
        
        //Asocio la venta a los productos
        for (int i = 0; i < productos.size(); i++) {
            // Agrego el ítem de la preferencia
            productos.get(i).setId_venta(id_venta);
        };
        System.out.println(productos);
        //Guardo los productos asociados a esa venta
        boolean result = pDAO.insertProductos(productos);
        System.out.println(result);
        JSONObject respuesta = new JSONObject().append("id_preferencia", preference.getId());
        return respuesta;
    };
    
    public static Route getBoton = (Request request, Response response) -> {
        HashMap model = new HashMap();
        String id_preferencia = request.queryParams("id_preferencia");
        
        MercadoPago.SDK.setAccessToken("TEST-6004468078258254-100814-9c68fc1b2328af18ac88457cbca9cf5c-190221195");
        
        PagoDAO pDAO = new PagoDAO();
        
        boolean isNull = pDAO.preferenceIsNULL(id_preferencia);
        if(isNull){
            JSONObject preference = new JSONObject(MercadoPago.SDK.Get("https://api.mercadopago.com/checkout/preferences/"+id_preferencia+"?access_token="+MercadoPago.SDK.getAccessToken().toString()).getStringResponse());
            System.out.println(preference);
            JSONObject persona = preference.getJSONObject("payer");
            
            List<Producto> productos = new ArrayList<>();
            JSONArray items = preference.getJSONArray("items");
            double sub_total = 0;
            for(int i = 0; i < items.length(); i++){
                JSONObject obj = items.getJSONObject(i);
                productos.add(new Producto(obj.getInt("quantity"),obj.getString("title"),obj.getDouble("unit_price")));
                if(i!=items.length()-1){
                    sub_total = sub_total + obj.getDouble("unit_price")*obj.getDouble("quantity");   
                }
            }
            double total = sub_total + items.getJSONObject(items.length()-1).getDouble("unit_price");//Referencio al ultimo producto, ya que se que es el iva
            
            
            model.put("nombre", persona.getString("name"));
            model.put("apellido", persona.getString("surname"));
            model.put("id_preferencia", preference.getString("id"));
            model.put("productos", productos);
            model.put("sub_total", sub_total);
            model.put("total", total);
            model.put("decimalFormat", new DecimalFormat("#.00"));
            
            return new VelocityTemplateEngine().render(new ModelAndView(model, "templates/pago.vsl")); 
        }else{
            return new VelocityTemplateEngine().render(new ModelAndView(model, "templates/error.vsl")); 
        }
    };
    
    public static Route autoReturn = (Request request, Response response) -> {
        String url = "http://localhost:4200/backurl/"+request.queryParams("payment_status");
        response.redirect(url);
        return null;
    };
}
