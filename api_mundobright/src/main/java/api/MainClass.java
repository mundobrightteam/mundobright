package api;

import spark.Filter;
import static spark.Spark.after;
import static spark.Spark.before;
import static spark.Spark.get;
import static spark.Spark.options;
import static spark.Spark.post;

public class MainClass {
    public static void main(String[] args){
           
        options("/*", (req, res) -> {
            String accessControlRequestHeaders = req.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                    res.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = req.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                    res.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            return "OK";
	});

	before((req, res) -> {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "*");
            res.type("application/json");
	});
       
        get("/productos",ControladorProducto.getProductos);
        get("/categorias",ControladorCategoria.getCategorias);
        get("/promociones",ControladorProducto.getPromociones);
        
        //if(sessionExists){
        get("/ventas",ControladorVenta.getVentas);
        get("/venta",ControladorVenta.getVenta);
        post("/promocion/set",ControladorPromocion.setPromocion);
        post("/promocion/update",ControladorPromocion.updatePromocion);
        post("/promocion/delete",ControladorPromocion.deletePromocion);
        //}
        
        
        post("/generar_preferencia",ControladorPago.getPreferencia);
        get("/generar_boton",ControladorPago.getBoton);
        get("/procesar-pago",ControladorPago.autoReturn);
        
        post("/login",ControladorSession.login);
        get("/autenticar",ControladorSession.autenticar);
        get("/cerrar_session",ControladorSession.cerrarSession);
    }
}
