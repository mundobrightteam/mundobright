package api;

import java.util.Date;
import java.util.List;
import org.sql2o.Connection;
import util.Sql2oDAO;

public class PromocionDAO {  
    public Promocion getPromocion(int id_producto) {
        String query = "SELECT id_promocion,precio_promocion,f1,f2 FROM promociones WHERE id_producto= :id_producto AND f1<NOW() AND f2>NOW()";
        List<Promocion> res = null;
        try (Connection con = Sql2oDAO.getSql2o().open()) {
           res = con.createQuery(query).addParameter("id_producto",id_producto).executeAndFetch(Promocion.class);
           return res.get(0);
        } catch(Exception e){
            System.out.println(e);
        }
        return null;
    }
    
    public Promocion getPromocionAdmin(int id_producto) {
        String query = "SELECT id_promocion,precio_promocion,f1,f2 FROM promociones WHERE id_producto= :id_producto";
        List<Promocion> res = null;
        try (Connection con = Sql2oDAO.getSql2o().open()) {
           res = con.createQuery(query).addParameter("id_producto",id_producto).executeAndFetch(Promocion.class);
           return res.get(0);
        } catch(Exception e){
            System.out.println(e);
        }
        return null;
    }
    
    public boolean setPromocion(int id_producto,Double precio_promocion,Date f1,Date f2) {
        String query = "INSERT INTO promociones (id_producto,precio_promocion,f1,f2) VALUES (:id_producto,:precio_promocion,:f1,:f2)";
        System.out.println(query);
        //try (Connection con = Conexion.getConexion()) {
        try (Connection con = Sql2oDAO.getSql2o().beginTransaction()) {
            con.createQuery(query)
                    .addParameter("id_producto", id_producto)
                    .addParameter("precio_promocion", precio_promocion)
                    .addParameter("f1", f1)
                    .addParameter("f2", f2)
                    .executeUpdate();
            con.commit();
            return true;
        } catch(Exception e){
            System.out.println(e);
        }
        
        return false;
    }
    
    public boolean updatePromocion(int id_producto,Double precio_promocion,Date f1,Date f2) {
        String query = "UPDATE promociones SET precio_promocion = :precio_promocion , f1 = :f1 , f2 = :f2 WHERE id_producto = :id_producto";
        System.out.println(query);
        //try (Connection con = Conexion.getConexion()) {
        try (Connection con = Sql2oDAO.getSql2o().beginTransaction()) {
            con.createQuery(query)
                    .addParameter("precio_promocion", precio_promocion)
                    .addParameter("f1", f1)
                    .addParameter("f2", f2)
                    .addParameter("id_producto", id_producto)
                    .executeUpdate();
            con.commit();
            return true;
        } catch(Exception e){
            System.out.println(e);
        }
        
        return false;
    }
    
    public boolean deletePromocion(int id_promocion) {
        String query = "DELETE FROM promociones WHERE id_promocion = :id_promocion";
        System.out.println(query);
        //try (Connection con = Conexion.getConexion()) {
        try (Connection con = Sql2oDAO.getSql2o().beginTransaction()) {
            con.createQuery(query)
                    .addParameter("id_promocion", id_promocion)
                    .executeUpdate();
            con.commit();
            return true;
        } catch(Exception e){
            System.out.println(e);
        }
        
        return false;
    }
}