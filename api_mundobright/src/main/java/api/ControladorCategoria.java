package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import spark.Request;
import spark.Response;
import spark.Route;

public class ControladorCategoria {
    public static Route getCategorias= (Request req, Response res)-> {
        CategoriaDAO categoriaDAO= new CategoriaDAO();
        List<Categoria> categorias = new ArrayList<>();
        categorias = categoriaDAO.getAll();
        ObjectMapper mapperObj = new​ ObjectMapper();
        String​ jsonStr​ = mapperObj.writeValueAsString(categorias);
        System.out.println(jsonStr);
        return​ jsonStr;
    };
}
