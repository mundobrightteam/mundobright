package util;


import java.util.*;
import org.sql2o.Sql2o;

public class Sql2oDAO {
 
    private static Sql2o sql2o;
    
    private Sql2oDAO(){};
    
    
    public static Sql2o getSql2o() {
        if (sql2o == null) {
            sql2o = new Sql2o("jdbc:mysql://localhost:3306/mundobright", "root", "toor");
        }
        return sql2o;
    }
}