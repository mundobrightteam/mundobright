package modelo;

import lombok.Data; 
import java.util.Date;

public class ProductoVenta {
    private Integer id_venta;
    private Integer id_producto;
    private Integer cantidad;
    private float precio_unitario;
    
    public ProductoVenta(Integer id_venta, Integer id_producto, Integer cantidad, Float precio_unitario){
        this.id_venta = id_venta;
        this.id_producto = id_producto;
        this.cantidad = cantidad;
        this.precio_unitario= precio_unitario;
    }
}
