package modelo;



import java.util.*;
import org.sql2o.Connection;
import util.Sql2oDAO;

/**
 * 
 */
public class ProductoDaoImplementacion implements ProductoDAO {

 
    public ProductoDaoImplementacion() {
    }


    public ArrayList<Producto> verTodosLosProductos() {
        String selectALLSQL = "SELECT * FROM PRODUCTOS;";
        List<Producto> res = null;
        try(Connection con = Sql2oDAO.getSql2o().open()){
            res = con.createQuery(selectALLSQL).executeAndFetch(Producto.class);
        }catch(Exception e){
            System.out.println(e);
        }
        return (ArrayList<Producto>) res;
    }
    
    public ArrayList<Producto> buscarProducto(String nombreProducto){
        String consulta = "SELECT * FROM PRODUCTO WHERE nombreProducto like '%"+nombreProducto+"%' OR titulo like '%"+nombreProducto+"%';";
        List<Producto> res=null;
        try(Connection con = Sql2oDAO.getSql2o().open()){
            res = con.createQuery(consulta).executeAndFetch(Producto.class);
            
        }catch(Exception e){
            System.out.println(e);
        }
        return (ArrayList<Producto>) res;
    
    }

    public ArrayList<Producto> verProducto(String idProducto) {
        String consulta = "SELECT * FROM PRODUCTOS WHERE idProducto="+idProducto+";";
        List<Producto> res=null;
        try(Connection con = Sql2oDAO.getSql2o().open()){
            res = con.createQuery(consulta).executeAndFetch(Producto.class);
            
        }catch(Exception e){
            System.out.println(e);
        }
        return (ArrayList<Producto>) res;
    }

    
}