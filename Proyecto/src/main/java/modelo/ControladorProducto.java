package modelo;



import java.util.ArrayList;
import java.util.HashMap;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.template.velocity.VelocityTemplateEngine;
import com.fasterxml.jackson​.databind​.ObjectMapper;
import com.fasterxml.jackson​.databind​.ObjectMapper;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.json.JSONObject;

/**
 * 
 */
public class ControladorProducto {


    public static Route inicio= (Request req, Response res)-> {
      
      
        ProductoDaoImplementacion productoDAO= new ProductoDaoImplementacion();
        
        HashMap model = new HashMap();
        model.put("productos", productoDAO.verTodosLosProductos());
        model.put("template", "templates/VerProductos1.vsl");
        return new VelocityTemplateEngine().render(new ModelAndView(model, "templates/index.vsl"));

    };
    
    
    public static Route buscarProducto= (Request req, Response res)-> {
        
        ArrayList<Producto> productos= new ArrayList<>();
        ProductoDaoImplementacion productoDAO= new ProductoDaoImplementacion();
        String nombreProducto= req.queryParams("nombreProducto");
        productos= productoDAO.buscarProducto(nombreProducto);
        
        
        HashMap model = new HashMap();
        if(productos.isEmpty()){
            model.put("template", "templates/noEncontrado.vsl");
        }else{
            model.put("productos", productos);
            model.put("template", "templates/listaProductos.vsl");
        }
        
        return new VelocityTemplateEngine().render(new ModelAndView(model, "templates/index.vsl"));

        // TODO implement here
    };

    /**
     * 
     */
    public static Route verProducto= (Request req, Response res)-> {
      
        ArrayList<Producto> productos= new ArrayList<>();
        ProductoDaoImplementacion productoDAO= new ProductoDaoImplementacion();
        String id= req.queryParams("id");
        productos= productoDAO.verProducto(id);
        
        
        //HashMap model = new HashMap();
        //model.put("producto",productos.get(0));
        //model.put("template", "templates/verProducto.vsl");
        //return new VelocityTemplateEngine().render(new ModelAndView(model, "templates/index.vsl"));
        
         ObjectMapper mapperObj = new​ ObjectMapper();
         String​ jsonStr​ = mapperObj.writeValueAsString(productos);
         System.out.println(jsonStr);
         return​ jsonStr;
         
         //JSONObject jsonObj = new JSONObject(productos);
          //  VelocityContext context = new VelocityContext();
           // context.put("json", jsonObj);  

        
// TODO implement here
    };

    
}