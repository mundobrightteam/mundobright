package modelo;


import java.util.*;

/**
 * 
 */
public interface ProductoDAO {

    public ArrayList<Producto> verTodosLosProductos();

    public ArrayList<Producto> buscarProducto(String nombreProducto);

    public  ArrayList<Producto>  verProducto(String idProducto);

}