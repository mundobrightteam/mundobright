package modelo;





import java.util.*;
import lombok.Data;

/**
 * 
 */
@Data
public class Producto {

    private Double precio;
    private int cantidad;
    private String marca;
    private String nombre;
    private String imagen;
    private int Id_sub_categoria;
    private int id_Producto;
    private Date fecha_publicacion;

    
    public Producto() {
    }

  
  

}