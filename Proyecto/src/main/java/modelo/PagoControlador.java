package modelo;

import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.Route;

// SDK de Mercado Pago
import com.mercadopago.MercadoPago;
import com.mercadopago.resources.Preference;
import com.mercadopago.resources.datastructures.preference.Address;
import com.mercadopago.resources.datastructures.preference.Identification;
import com.mercadopago.resources.datastructures.preference.Item;
import com.mercadopago.resources.datastructures.preference.Payer;
import com.mercadopago.resources.datastructures.preference.Phone;
import java.util.ArrayList;
import java.util.List;

public class PagoControlador {
    
    public static Route getPage = (Request request, Response response) -> {
        HashMap model = new HashMap();
        
        String nombre = (String)request.queryParams("nombre");
        String apellido = (String)request.queryParams("apellido");
        String email = (String)request.queryParams("email");
        String dni = (String)request.queryParams("dni");
        String telefono = (String)request.queryParams("telefono");
        String direccion = (String)request.queryParams("direccion");
        String numero = (String)request.queryParams("numero");
        String codigo_postal = (String)request.queryParams("codigo_postal");
        
        
        System.out.println(nombre);
        System.out.println(apellido);
        System.out.println(email);
        System.out.println(dni);
        System.out.println(telefono);
        System.out.println(direccion);
        System.out.println(numero);
        System.out.println(codigo_postal);
        
        
        // Agrega credenciales
        //MercadoPago.SDK.setAccessToken("TEST-6004468078258254-100814-9c68fc1b2328af18ac88457cbca9cf5c-190221195");
        
        // Crea un objeto de preferencia
        //Preference preference = new Preference();
        
        //Creo el payer
        /*
        Payer payer = new Payer();
        payer.setName(nombre)
            .setSurname(apellido)
            .setEmail(email)
            .setDateCreated("2018-06-02T12:58:41.425-04:00")
            .setPhone(new Phone()
                .setAreaCode(area_code)
                .setNumber(numero))
            .setIdentification(new Identification()
                .setType("DNI")
                .setNumber(dni))
            .setAddress(new Address()
                .setStreetName(direccion)
                .setStreetNumber(Integer.valueOf(numero_casa))
                .setZipCode(codigo_postal));
        */
        // Crea un ítem en la preferencia
        /*
        Item item = new Item();
        item.setTitle("Carrito BB")
            .setQuantity(1)
            .setUnitPrice((float) 75.56);
        
        preference.appendItem(item);
        
        preference.setPayer(payer);
        preference.save();
        
        PagoDAO pDAO = new PagoDAO();
        */
        //Guardo la venta
        /*
        Integer id_venta = pDAO.insertVenta(Integer.valueOf(preference.getId()));
        
        List<ProductoVenta> productos = new ArrayList<>();;
        productos.add(new ProductoVenta(id_venta,1,1,(float) 75.56));
        productos.add(new ProductoVenta(id_venta,2,4,(float) 15.56));
        */
        //Guardo los productos asociados a esa venta
        //boolean result = pDAO.insertProductos(productos);
        
        //System.out.println(preference.getId());
        return "";
    };
}
