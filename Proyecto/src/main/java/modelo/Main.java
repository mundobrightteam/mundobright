/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
import static spark.Spark.get;
import static spark.Spark.staticFiles;

import spark.Filter;
import static spark.Spark.after;
import static spark.Spark.post;

    
public class Main {
    
    
   public static void main(String[] args){
               //Por CORS
        after((Filter) (request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            //response.header("Access-Control-Allow-Methods", "GET");
        });
       
        staticFiles.location("/templates");
         //  CorsFilter.apply(); // Call this before mapping thy routes
           get("/verproducto",ControladorProducto.verProducto);
           //get("/buscarproducto",ControladorProducto.buscarProducto);
           get("/",ControladorProducto.inicio);
          // get("/agregarProductoAlCarrito",ControladorCarrito.agregarProductoAlCarrito);
           //get("/borrarDatosCarrito",ControladorCarrito.borrarDatosCarrito);
           
           post("/generar_boton_compra",PagoControlador.getPage);
           
   }

    
    
}
