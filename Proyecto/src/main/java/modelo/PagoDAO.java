package modelo;

import com.mercadopago.resources.datastructures.preference.Item;
import java.util.List;
import org.sql2o.Connection;
import util.Sql2oDAO;

public class PagoDAO {
    
    public Integer insertVenta(Integer id_referencia) {
        String query = "INSERT INTO ventas (id_referencia) VALUES (:id_referencia)";
        System.out.println(query);
        //try (Connection con = Conexion.getConexion()) {
        try (Connection con = Sql2oDAO.getSql2o().open()) {
             con.createQuery(query).bind(id_referencia).executeUpdate();
        } catch(Exception e){
            //registraLog.error("Error al Insertar con {}", query, e);
        }
        //registraLog.info("FINALIZO LA INSERCION {}", query);

        query = "SELECT MAX(id_venta) FROM ventas;";
        System.out.println(query);
        try (Connection con = Sql2oDAO.getSql2o().open()) {
             List<Integer> resp = con.createQuery(query).executeAndFetch(Integer.class);
             return resp.get(0);
        } catch(Exception e){
            //registraLog.error("Error al Seleccionar con {}", query, e);
        }
        
        return null;
    }
    
    public boolean insertProductos(List<ProductoVenta> productos) {
        String query = "INSERT INTO productos_x_ventas (id_venta,id_producto,cantidad,precio_unitario) VALUES (:id_venta,:id_producto,:cantidad,:precio_unitario)";
        System.out.println(query);
        //try (Connection con = Conexion.getConexion()) {
        try (Connection con = Sql2oDAO.getSql2o().open()) {
            for(ProductoVenta producto : productos){
                con.createQuery(query).bind(producto).executeUpdate();
            }
            return true;
        } catch(Exception e){
            //registraLog.error("Error al Insertar con {}", query, e);
        }
        //registraLog.info("FINALIZO LA INSERCION {}", query);
        
        return false;
    }
    
}
