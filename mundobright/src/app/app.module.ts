import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//Servicio para compartir datos entre componentes
import { SharingDataService } from "./sharingData.service";

//Para hacer andar el ngModule
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

//Angular Material
import { AngularMaterialModule } from './angular-material.module';


//Http para hacer peticiones a API
import { HttpClientModule } from "@angular/common/http";
import { ApiHTTP } from "./http.service";




//Enrutador
import { RouterModule, Route } from '@angular/router';

const routes: Route[] = [
	{ path: '', component: Busqueda },
	{ path: 'carrito', component: Carrito },
	{ path: 'backurl/:view', component: Backurl },
	{ path: 'administrador', component: Administrador,children:[
		{ path: '', component: Ventas },
		{ path: 'ventas', component: Ventas },
		{ path: 'ventas/:id', component: Ventas },
		{ path: 'promociones', component: Promociones }
	]}
];

import { AppComponent } from './app.component';

import { Busqueda } from "./busqueda/busqueda.component";
import { Carrito } from "./carrito/carrito.component";
import { Backurl } from "./backurl/backurl.component";
import { Administrador } from "./administrador/administrador.component";
import { Ventas } from "./administrador/ventas/ventas.component";
import { Promociones } from "./administrador/promociones/promociones.component"

@NgModule({
	declarations: [
		AppComponent,
		Busqueda,
		Carrito,
		Backurl,
		Ventas,
		Promociones,
		Administrador
	],
	imports: [
		BrowserModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule.forRoot(routes),
		HttpClientModule,
		AngularMaterialModule
	],
	providers: [SharingDataService,ApiHTTP],
	bootstrap: [AppComponent]
})
export class AppModule { }
