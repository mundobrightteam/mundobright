import { Component, OnInit } from '@angular/core';

//Para llamadas a la API
import { ApiHTTP } from "./http.service";
import { HttpHeaders } from '@angular/common/http';

import { Router } from '@angular/router';

//Compartir informacion entre componentes
import { SharingDataService } from "./sharingData.service";
import { Subscription } from "rxjs";

import { environment } from "../environments/environment";

//JQUERY
import * as $ from 'jquery';
import { Categoria } from './interfaces';

export interface Producto {
    id_producto: number;
    nombre: string;
    precio: number;
    cantidad: number;
    marca: string;
    imagen: string;
    id_sub_categoria: number;
    fecha_publicacion: Date;
    id_localstorage: number;
    cant_productos: number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
    //Atributos
    dataSubscription: Subscription;
    data: Array<string> = [];


    categorias: Categoria[] = [];

    //Constructor
    constructor(public _sharingDataService:SharingDataService,public _router:Router, public http: ApiHTTP){
        console.log("Ejecucion --> Constructor Componente AppComponent");
        this.dataSubscription = this._sharingDataService.dataSource$.subscribe(data => { this.data = data; });
    }

    //Metodos
    public ngOnInit() {
        console.log("Ejecucion --> ngOnInit Componente AppComponent");
        this.data['httpOptions'] = { headers: new HttpHeaders({}), withCredentials : false};//{ headers: new HttpHeaders({ 'Content-Type': 'application/json' }), withCredentials : false};
        this.data['cant_productos_carrito'] = this.getProductosFromLocalStorage().length;
        this.data["administrador"] = false;
        this._sharingDataService.setData(this.data);

        if(environment.admin){
            this._router.navigate(['/administrador/']);
        }

        // Chekeo estado de la session, en caso de estar iniciada activo sus respectivos servicios
        
        this.http.get("/autenticar",this.data['httpOptions']).subscribe(result => {
            if(result["value"]){
                this.data['nombre']=result["nombre"];
            }
            this.data['administrador']=result["value"];
            this._sharingDataService.setData(this.data);
        },error => {
            console.log('Ah ocurrido un error')
        });

        this.getCategorias();
    }

    public getProductosFromLocalStorage() {
        var productos: Producto[] = [];
        if (JSON.parse(localStorage.getItem('productos')) != null) {
            productos = JSON.parse(localStorage.getItem('productos'));
        }
        return productos;
    }

    public cerrarSesion(){
        this.data["administrador"] = false;
    }

    public getCategorias(){
        this.http.get("/categorias", this.data['httpOptions']).subscribe(result => {
            let respuesta;
            respuesta = result;
            this.categorias = respuesta;
        }, error => {
            console.log('Ah ocurrido un error')
        });
    }

    public ngOnDestroy(): void {
        console.log("Destruccion --> ngOnDestroy Componente AppComponent");
        this.dataSubscription.unsubscribe();
    }
}
