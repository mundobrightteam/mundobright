import { Component, OnInit } from '@angular/core';

//Para llamadas a la API
import { ApiHTTP } from "../http.service";

//JQUERY
import * as $ from 'jquery';

//Compartir informacion entre componentes
import { SharingDataService } from "../sharingData.service";
import { Subscription } from "rxjs";

//Variables de entorno
import { environment } from '../../environments/environment';

import {Producto} from '../interfaces';

@Component({
    selector: 'carrito',
    templateUrl: './carrito.component.html',
    styleUrls: ['./carrito.component.css']
})

export class Carrito implements OnInit {

    //Atributos
    dataSubscription: Subscription;
    data: Array<string> = [];

    productos: Producto[] = [];


    view = 1;
    loader=true;
    mensaje_loader="Cargando...";

    info_payer = {
        email:"",
        nombre:"",
        apellido:"",
        telefono:"",
        dni:"",
        retiro:"",
        codigo_postal:"",
        numero:"",
        direccion:"",
        ciudad:"",
        respuesta_url:""
    };

    sub_total = {
        entero: "0",
        decimal: "00"
    }

    total = {
        entero: "0",
        decimal: "00"
    }

    //Constructor
    constructor(public _sharingDataService:SharingDataService, public http: ApiHTTP){
        console.log("Ejecucion --> Constructor Componente Carrito");

        this.cleanStorge();
    }

    //Metodos
    public ngOnInit() {
        console.log("Ejecucion --> ngOnInit Componente Carrito");

        this.getProductos();
        this.calcularTotales();
        this.dataSubscription = this._sharingDataService.dataSource$.subscribe(data => { this.data = data; });
    }

    public cleanStorge(){
        var productos: Producto[] = this.getProductosFromLocalStorage();
        var aux: Producto[] = [];
        for(let prod of productos){
            if(prod.cant_productos!=null){
                aux.push(prod);
            }
        }
        localStorage.setItem('productos', JSON.stringify(aux));
        this.updateContProductos(0);
    }

    public calcularTotales(){
        var sub_total:number = 0;
        var total:number = 0;
        for(let prod of this.productos){
            sub_total = sub_total + (prod.precio * prod.cant_productos);
        }
        total = sub_total + (sub_total*0.21);
        var aux=parseFloat(String(Math.round(total * 100) / 100)).toFixed(2);
        total = parseFloat(aux);
        aux=parseFloat(String(Math.round(sub_total * 100) / 100)).toFixed(2);
        sub_total = parseFloat(aux);
        var arr_sub_total = String(sub_total).split(".");
        var arr_total = String(total).split(".");

        this.sub_total.entero = arr_sub_total[0];
        if(arr_sub_total.length>1){
            this.sub_total.decimal = arr_sub_total[1].slice(0, 2);
        }else{
            this.sub_total.decimal = "00";
        }

        this.total.entero = arr_total[0];
        if(arr_total.length>1){
            this.total.decimal = arr_total[1].slice(0, 2);
        }else{
            this.total.decimal = "00";
        }
    }

    public siguiente(){
        this.view = this.view + 1;
    }

    public setRetiro(retiro: string){
        this.info_payer['retiro'] = retiro;
    }

    public generar_preferencia(){
        let sanitizacion = true;
        if(this.info_payer.nombre=="" ){
            sanitizacion = false;
            alert("nombre incorrecto");
        }
        if(this.info_payer.apellido==""){
            sanitizacion = false;
            alert("apellido incorrecto");
        }
        if(this.info_payer.dni==""){
            sanitizacion = false;
            alert("dni incorrecto");
        }
        if(this.info_payer.email==""){
            sanitizacion = false;
            alert("email incorrecto");
        }
        if(this.info_payer.telefono==""){
            sanitizacion = false;
            alert("telefono incorrecto");
        }
        if(this.info_payer.retiro==""){
            sanitizacion = false;
            alert("tipo retiro incorrecto");
        }
        if(this.info_payer.ciudad==""){
            sanitizacion = false;
            alert("ciudad incorrecto");
        }
        if(this.info_payer.codigo_postal==""){
            sanitizacion = false;
            alert("codigo_postal incorrecto");
        }
        if(this.info_payer.direccion==""){
            sanitizacion = false;
            alert("direccion incorrecto");
        }
        if(this.info_payer.numero==""){
            sanitizacion = false;
            alert("numero incorrecto");
        }

        if(sanitizacion){
            this.siguiente();
            var data = {};
            this.info_payer.respuesta_url = environment.web_url;
            data['info_payer'] = this.info_payer;
            data['items'] = [];
            for(let x of this.productos){
                data['items'].push({'id_producto':x.id_producto,'cantidad':x.cant_productos,'precio':x.precio,'nombre':x.nombre});
            }
            this.mensaje_loader="Cargando pasarela de pago...";
            this.loader=true;
            this.http.post("/generar_preferencia", data, this.data['httpOptions']).subscribe(result => {
                let id_preferencia = result["id_preferencia"][0];
                window.location.href = environment.api_url+'/generar_boton?id_preferencia='+id_preferencia;
            },error => {
                console.log('Ah ocurrido un error');
            });
        }
    }

    public atras(){
        this.view = this.view - 1;
    }

    public deleteProducto(index_localstorage: number,index_productos: number) {
        var productos: Producto[] = this.getProductosFromLocalStorage();
        productos[index_localstorage].cant_productos = null;
        this.productos = this.productos.filter(item => item !== this.productos[index_productos]);
        localStorage.setItem('productos', JSON.stringify(productos));
        //this.cleanStorge();
        this.updateContProductos(-1);
        this.calcularTotales();
    }

    public getProductos() {
        let prods = this.getProductosFromLocalStorage();
        prods.forEach((producto, index) => {
            if (producto.promocion!=null){
                producto.precio = producto.promocion.precio_promocion;
            }
            producto['id_localstorage']=index;
            producto['cant_productos'] = prods[index].cant_productos;
            this.productos.push(producto);
        });
        this.loader=false;
    }

    public updateContProductos(x: number){
        if(x!=0){
            this.data['cant_productos_carrito'] = this.data['cant_productos_carrito'] + x;
        }else{
            this.data['cant_productos_carrito'] = this.getProductosFromLocalStorage().length;
        }
        this._sharingDataService.setData(this.data);
    }

    public aumentarCantidad(index_localstorage: number,index_productos: number) {
        if(index_localstorage==null){
            if(this.productos[index_productos].cant_productos<=this.productos[index_productos].cantidad){
                this.productos[index_productos].cant_productos = this.productos[index_productos].cant_productos+1;
            }
        }else{
            if(this.productos[index_productos].cant_productos<this.productos[index_productos].cantidad){
                var productos: Producto[] = this.getProductosFromLocalStorage();
                productos[index_localstorage].cant_productos = productos[index_localstorage].cant_productos+1;
                this.productos[index_productos].cant_productos = productos[index_localstorage].cant_productos;
                localStorage.setItem('productos', JSON.stringify(productos));
            }
        }
        this.calcularTotales();
    }

    public disminuirCantidad(index_localstorage: number,index_productos: number) {
        if(index_localstorage==null){
            if(this.productos[index_productos].cant_productos>1){
                this.productos[index_productos].cant_productos = this.productos[index_productos].cant_productos-1;
            }
        }else{
            var productos: Producto[] = this.getProductosFromLocalStorage();
            if(productos[index_localstorage].cant_productos>1){
                productos[index_localstorage].cant_productos = productos[index_localstorage].cant_productos-1;
                this.productos[index_productos].cant_productos = productos[index_localstorage].cant_productos;
                localStorage.setItem('productos', JSON.stringify(productos));
            }
        }
        this.calcularTotales();
    }

    public getProductosFromLocalStorage() {
        var productos: Producto[] = [];
        if (JSON.parse(localStorage.getItem('productos')) != null) {
            productos = JSON.parse(localStorage.getItem('productos'));
        }
        return productos;
    }

    public ngOnDestroy(): void {
        console.log("Destruccion --> ngOnDestroy Componente Carrito");
        this.dataSubscription.unsubscribe();
    }

}