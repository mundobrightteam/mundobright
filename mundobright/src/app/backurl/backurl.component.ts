import { Component, OnInit } from '@angular/core';

//Para llamadas a la API
import { ApiHTTP } from "../http.service";

//Compartir informacion entre componentes
import { SharingDataService } from "../sharingData.service";
import { Subscription } from "rxjs";

import { ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: 'backurl',
    templateUrl: './backurl.component.html',
    styleUrls: ['./backurl.component.css']
})

export class Backurl implements OnInit {

    //Variables de control
    vista = "";
    loader = true;
    //Atributos
    dataSubscription: Subscription;
    data: Array<string> = [];


    //Constructor
    constructor(private rutaActiva:ActivatedRoute,public _sharingDataService: SharingDataService, public http: ApiHTTP) {
        console.log("Ejecucion --> Constructor Componente Backurl");
        this.dataSubscription = this._sharingDataService.dataSource$.subscribe(data => { this.data = data; });
    }

    //Metodos
    public ngOnInit() {
        console.log("Ejecucion --> ngOnInit Componente Backurl");

        this.rutaActiva.params.subscribe(
            (params: Params) => {
                this.vista = params["view"];
                if(this.vista=="success" || this.vista=="approved" || this.vista=="pending"){
                    localStorage.clear();
                }
                this.loader = false;
            }
        );
    }

    public ngOnDestroy(): void {
        console.log("Destruccion --> ngOnDestroy Componente Backurl");
        this.dataSubscription.unsubscribe();
    }

}