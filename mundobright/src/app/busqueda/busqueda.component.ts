import { Component, OnInit } from '@angular/core';

//Para llamadas a la API
import { ApiHTTP } from "../http.service";

//Compartir informacion entre componentes
import { SharingDataService } from "../sharingData.service";
import { Subscription } from "rxjs";

import {Producto} from '../interfaces';


@Component({
    selector: 'busqueda',
    templateUrl: './busqueda.component.html',
    styleUrls: ['./busqueda.component.css']
})

export class Busqueda implements OnInit {

    //Variables de control
    vista: number = 1;
    loader = true;

    //Atributos
    dataSubscription: Subscription;
    data: Array<string> = [];

    productos: Producto[] = [];

    //Constructor
    constructor(public _sharingDataService: SharingDataService, public http: ApiHTTP) {
        console.log("Ejecucion --> Constructor Componente Busqueda");
        this.dataSubscription = this._sharingDataService.dataSource$.subscribe(data => { this.data = data; });
        this.getVista();
        this.cleanStorge();
    }

    //Metodos
    public ngOnInit() {
        console.log("Ejecucion --> ngOnInit Componente Busqueda");

        if(window.innerWidth<=768){
            this.vista=2;
        }
        this.getProductos();
    }

    public changeVista(x: number) {
        this.vista = x;
        localStorage.setItem('vista',JSON.stringify(x));
    }

    public getVista(){
        var vista = JSON.parse(localStorage.getItem('vista'));
        if (vista == null) {
            localStorage.setItem('vista',JSON.stringify(1));
            vista = 1;
        }
        this.vista = vista;
    }

    public getProductos() {
        this.http.get("/productos", this.data['httpOptions']).subscribe(result => {
            let respuesta;
            respuesta = result;

            //Metodo de busqueda
            const customFilter = (productos, producto) => {
                const result = [];
                productos.forEach((element, index) => {
                    if (element.id_producto == producto.id_producto) {
                        result.push(index);
                    }
                });

                return result;
            }
            //---------------------
            var productos: Producto[] = this.getProductosFromLocalStorage();
            for (let producto of respuesta) {
                var arr = customFilter(productos,producto);
                if (arr.length == 1) {
                    producto['id_localstorage']=arr[0];
                    producto['cant_productos'] = productos[arr[0]].cant_productos;
                } else {
                    producto['id_localstorage'] = null;
                    producto['cant_productos'] = 1;
                }

                this.productos.push(producto);
            }
            this.loader = false;
        }, error => {
            console.log('Ah ocurrido un error')
        });
    }

    public cleanStorge(){
        var productos: Producto[] = this.getProductosFromLocalStorage();
        var aux: Producto[] = [];
        for(let prod of productos){
            if(prod.cant_productos!=null){
                aux.push(prod);
            }
        }
        localStorage.setItem('productos', JSON.stringify(aux));
        this.updateContProductos(0);
    }

    public getProductosFromLocalStorage() {
        var productos: Producto[] = [];
        if (JSON.parse(localStorage.getItem('productos')) != null) {
            productos = JSON.parse(localStorage.getItem('productos'));
        }
        return productos;
    }

    public addProducto(index_productos: number) {
        var productos: Producto[] = this.getProductosFromLocalStorage();
        this.productos[index_productos].id_localstorage = productos.length;
        productos.push(this.productos[index_productos]);
        localStorage.setItem('productos', JSON.stringify(productos));
        this.updateContProductos(1);
    }

    public deleteProducto(index_localstorage: number,index_productos: number) {
        var productos: Producto[] = this.getProductosFromLocalStorage();
        productos[index_localstorage].cant_productos = null;
        this.productos[index_productos].id_localstorage = null;
        this.productos[index_productos].cant_productos = 1;
        localStorage.setItem('productos', JSON.stringify(productos));
        this.updateContProductos(-1);
    }

    public aumentarCantidad(index_localstorage: number,index_productos: number) {
        if(index_localstorage==null){
            if(this.productos[index_productos].cant_productos<=this.productos[index_productos].cantidad){
                this.productos[index_productos].cant_productos = this.productos[index_productos].cant_productos+1;
            }
        }else{
            if(this.productos[index_productos].cant_productos<this.productos[index_productos].cantidad){
                var productos: Producto[] = this.getProductosFromLocalStorage();
                productos[index_localstorage].cant_productos = productos[index_localstorage].cant_productos+1;
                this.productos[index_productos].cant_productos = productos[index_localstorage].cant_productos;
                localStorage.setItem('productos', JSON.stringify(productos));
            }
        }
    }

    public disminuirCantidad(index_localstorage: number,index_productos: number) {
        if(index_localstorage==null){
            if(this.productos[index_productos].cant_productos>1){
                this.productos[index_productos].cant_productos = this.productos[index_productos].cant_productos-1;
            }
        }else{
            var productos: Producto[] = this.getProductosFromLocalStorage();
            if(productos[index_localstorage].cant_productos>1){
                productos[index_localstorage].cant_productos = productos[index_localstorage].cant_productos-1;
                this.productos[index_productos].cant_productos = productos[index_localstorage].cant_productos;
                localStorage.setItem('productos', JSON.stringify(productos));
            }
        }
    }

    public updateContProductos(x: number){
        if(x!=0){
            this.data['cant_productos_carrito'] = this.data['cant_productos_carrito'] + x;
        }else{
            this.data['cant_productos_carrito'] = this.getProductosFromLocalStorage().length;
        }
        this._sharingDataService.setData(this.data);
    }

    public ngOnDestroy(): void {
        console.log("Destruccion --> ngOnDestroy Componente Busqueda");
        this.dataSubscription.unsubscribe();
    }

}