import { Component, OnInit } from '@angular/core';

//Para llamadas a la API
import { ApiHTTP } from "../../http.service";

//Compartir informacion entre componentes
import { SharingDataService } from "../../sharingData.service";
import { Subscription } from "rxjs";

//Para recibir las rutas del tipo ventas/:id
import { ActivatedRoute, Params } from '@angular/router';
import { Venta } from '../../interfaces';

import { Router } from '@angular/router';

@Component({
    selector: 'ventas',
    templateUrl: './ventas.component.html',
    styleUrls: ['./ventas.component.css']
})

export class Ventas implements OnInit {

    //Variables de control
    loader = true;
    gral=true;

    //Atributos
    dataSubscription: Subscription;
    data: Array<string> = [];

    ventas: Venta[] = [];

    infoVenta={
        expired: false,
        nombre: "",
        apellido: "",
        dni : "",
        correo : "",
        tel : "",
        calle : "",
        nro_calle : "",
        cp : "",
        items: [],
        total: 0
    }

    //Constructor
    constructor(private rutaActiva:ActivatedRoute,public _router:Router,public _sharingDataService: SharingDataService, public http: ApiHTTP) {
        console.log("Ejecucion --> Constructor Componente Ventas");
        this.dataSubscription = this._sharingDataService.dataSource$.subscribe(data => { this.data = data; });
    }

    //Metodos
    public ngOnInit() {
        console.log("Ejecucion --> ngOnInit Componente Ventas");

        this.rutaActiva.params.subscribe(
            (params: Params) => {
                this.loader = true;
                if(params["id"]!=null){
                    this.gral=false;
                    this.getVenta(params["id"]);
                }else{
                    this.gral=true;
                    this._router.navigate(['/administrador/ventas']);
                    this.getVentas();
                }
            }
        );
    }

    public getVenta(idVenta:number) {
        this.http.get("/venta?id_venta="+idVenta, this.data['httpOptions']).subscribe(result => {
            this.infoVenta.expired = result["expired"];
            this.infoVenta.nombre = result["payer"]["name"];
            this.infoVenta.apellido = result["payer"]["surname"];
            this.infoVenta.dni = result["payer"]["identification"]["number"];
            this.infoVenta.correo = result["payer"]["email"];
            this.infoVenta.tel = result["payer"]["phone"]["number"];
            this.infoVenta.calle = result["payer"]["address"]["street_name"];
            this.infoVenta.nro_calle = result["payer"]["address"]["street_number"];
            this.infoVenta.cp = result["payer"]["address"]["zip_code"];
            this.infoVenta.items = result["items"];
            this.infoVenta.total = 0;
            this.infoVenta.items.forEach(item => {
                this.infoVenta.total = this.infoVenta.total + item.unit_price;
            });
            this.loader = false;
        }, error => {
            console.log('Ah ocurrido un error')
        });
    }

    public getVentas() {
        this.http.get("/ventas", this.data['httpOptions']).subscribe(result => {
            let respuesta;
            respuesta = result;

            //---------------------
            for (let venta of respuesta) {
                var d = new Date(venta["fecha"]);
                venta["fecha"] = d.getDate()+"/"+d.getMonth()+"/"+d.getFullYear()
                this.ventas.push(venta);
            }
            this.loader = false;
        }, error => {
            console.log('Ah ocurrido un error')
        });
    }

    public ngOnDestroy(): void {
        console.log("Destruccion --> ngOnDestroy Componente Ventas");
        this.dataSubscription.unsubscribe();
    }

}