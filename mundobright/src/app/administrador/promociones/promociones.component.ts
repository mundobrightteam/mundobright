import { Component, OnInit } from '@angular/core';

//Para llamadas a la API
import { ApiHTTP } from "../../http.service";

//Compartir informacion entre componentes
import { SharingDataService } from "../../sharingData.service";
import { Subscription } from "rxjs";

import {coerceNumberProperty} from '@angular/cdk/coercion';

import {ProductoPromocion} from '../../interfaces';

@Component({
    selector: 'promociones',
    templateUrl: './promociones.component.html',
    styleUrls: ['./promociones.component.css']
})

export class Promociones implements OnInit {

    //Variables de Control
    loader:Boolean = true;
    edit:Boolean = false;
    add:Boolean = false;
    today:Date = new Date();
    f2:Date;
    precio_edit:number = 0;


    //Atributos
    dataSubscription: Subscription;
    data: Array<string> = [];

    productos: ProductoPromocion[] = [];

    //Constructor
    constructor(public _sharingDataService: SharingDataService, public http: ApiHTTP) {
        console.log("Ejecucion --> Constructor Componente Promociones");
        this.dataSubscription = this._sharingDataService.dataSource$.subscribe(data => { this.data = data; });
    }

    //Metodos
    public ngOnInit() {
        console.log("Ejecucion --> ngOnInit Componente Promociones");

        this.loader = false;

        this.selectAllProducts();

    }

    public selectAllProducts(){
        this.http.get("/promociones", this.data['httpOptions']).subscribe(result => {
            let respuesta;
            respuesta = result;

            for (let producto of respuesta) {
                if(producto.promocion != null){
                    producto.promocion.f1 = new Date(producto.promocion.f1);
                    producto.promocion.f2 = new Date(producto.promocion.f2);
                }
                this.productos.push(producto);
            }
            this.loader = false;
        }, error => {
            console.log('Ah ocurrido un error');
        });
    }

    public editar(id){
        this.productos[id].edit = true;
        this.precio_edit = this.productos[id].promocion.precio_promocion;
    }

    public eliminar(id){
        //Elimino promocion y traigo el precio original de la base de datos
        var data = {"id_promocion":this.productos[id].promocion.id_promocion};
        this.http.post("/promocion/delete", data, this.data['httpOptions']).subscribe(result => {
            this.productos[id].promocion = null;
        },error => {
            console.log('Ah ocurrido un error');
        });
    }

    public agregar(id){
        this.productos[id].promocion = {id_promocion: 0, precio_promocion: 0, f1: new Date(),f2: new Date()};
        this.productos[id].add = true;
        this.productos[id].edit = false;
        this.precio_edit = this.productos[id].precio;
    }

    public guardar(id){
        this.productos[id].add = false;
        this.productos[id].promocion.precio_promocion = this.precio_edit;
        var data = {"id_producto":this.productos[id].id_producto,"precio_promocion":this.productos[id].promocion.precio_promocion,"f1":this.productos[id].promocion.f1,"f2":this.productos[id].promocion.f2};
        this.http.post("/promocion/set", data, this.data['httpOptions']).subscribe(result => {
            //console.log(result); Revisar por que retorna null
        },error => {
            console.log('Ah ocurrido un error');
        });
    }

    public cancelaradd(id){
        this.productos[id].add = false;
        this.productos[id].promocion = null;
    }

    public guardaredit(id){
        this.productos[id].edit = false;
        this.productos[id].promocion.precio_promocion = this.precio_edit;
        var data = {"id_producto":this.productos[id].id_producto,"precio_promocion":this.productos[id].promocion.precio_promocion,"f1":this.productos[id].promocion.f1,"f2":this.productos[id].promocion.f2};
        this.http.post("/promocion/update", data, this.data['httpOptions']).subscribe(result => {
            if(result != null){
                this.productos[id].promocion.id_promocion = result["id_promocion"];
            }
        },error => {
            console.log('Ah ocurrido un error');
        });
    }

    public cancelaredit(id){
        this.productos[id].promocion.f2 = this.f2;
        this.productos[id].edit = false;
    }
    
    public ngOnDestroy(): void {
        console.log("Destruccion --> ngOnDestroy Componente Promociones");
        this.dataSubscription.unsubscribe();
    }

    

}