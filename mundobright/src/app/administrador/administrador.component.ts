import { Component, OnInit } from '@angular/core';

//Para llamadas a la API
import { ApiHTTP } from "../http.service";

//Compartir informacion entre componentes
import { SharingDataService } from "../sharingData.service";
import { Subscription } from "rxjs";

@Component({
    selector: 'administrador',
    templateUrl: './administrador.component.html',
    styleUrls: ['./administrador.component.css']
})

export class Administrador implements OnInit {
    //Atributos
    dataSubscription: Subscription;
    data: Array<string> = [];

    loginData = {
        correo: "",
        password: ""
    }


    //Constructor
    constructor(public _sharingDataService: SharingDataService, public http: ApiHTTP) {
        console.log("Ejecucion --> Constructor Componente Administrador");
        this.dataSubscription = this._sharingDataService.dataSource$.subscribe(data => { this.data = data; });
    }

    //Metodos
    public ngOnInit() {
        console.log("Ejecucion --> ngOnInit Componente Administrador");
        
        this.data['nombre'] = "Nahuel";
        //this.data["administrador"] = true;
        this._sharingDataService.setData(this.data);
        //this.session = this.data['administrador'];
    }

    public login(){
        this.http.post("/login", this.loginData, this.data['httpOptions']).subscribe(result => {
            if(result["value"]==true){
                this.data['nombre']=result["nombre"];
                this.data['administrador']=true;
                this._sharingDataService.setData(this.data);
            }else{
                alert(result["mensaje"]);
            }
        },error => {
            console.log('Ah ocurrido un error');
        });
    }

    public ngOnDestroy(): void {
        console.log("Destruccion --> ngOnDestroy Componente Administrador");
        this.dataSubscription.unsubscribe();
    }

}