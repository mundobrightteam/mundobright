export interface Categoria {
    id_categoria: number;
    nombre: string;
}

export interface Producto {
    id_producto: number;
    nombre: string;
    precio: number;
    cantidad: number;
    marca: string;
    imagen: string;
    id_sub_categoria: number;
    fecha_publicacion: Date;
    id_localstorage: number;
    cant_productos: number;
    promocion: Promocion;
}

export interface ProductoPromocion extends Producto{
    edit: boolean;
    add: boolean;
}

export interface Promocion {
    id_promocion: number;
    precio_promocion: number;
    f1: Date;
    f2: Date;
}

export interface Venta {
    id_venta:number;
    id_preferencia: String;
    metodo_retiro: String;
    fecha: Date;
    nombre:String
    pago:Boolean;
    calle:String;
    nro_calle:number;
    cp:String;
    tel:number;
    total:DoubleRange;
}