import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';

@Injectable()
export class ApiHTTP {

    private urlApi:string;

    constructor(public http:HttpClient){
        this.urlApi = environment.api_url;
    }

    public get(condicion:string,headers){
        return this.http.get(this.urlApi+condicion,headers);
    }

    public post(condicion:string,data,headers){
        return this.http.post(this.urlApi+condicion,data,headers);
    }
}