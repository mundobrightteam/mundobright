import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class SharingDataService {
    data: Array<any> = [];

    public _dataSource = new BehaviorSubject<Array<any>>([]);

    dataSource$ = this._dataSource.asObservable();

    public setData(data:Array<any>){
        this.data = data;
        this._dataSource.next(data);
    }       
}