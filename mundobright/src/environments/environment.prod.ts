export const environment = {
  production: true,
  api_url: "http://localhost:4567",
  web_name: "MundoBright",
  files_url: "http://localhost/mundobright/files",
  version:"1.0.0a",
  idioma:"es",
  web_url:"http://localhost:4200",
  admin: false
};
