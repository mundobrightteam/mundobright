// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
/*
export const environment = {
  production: true,
  api_url: "http://localhost:4567",
  web_name: "MundoBright",
  files_url: "http://localhost/mundobright/files",
  version:"1.0.0a",
  idioma:"es",
  web_url:"https://localhost:4200"
};*/

export const environment = {
  production: true,
  api_url: "http://localhost:4567",
  web_name: "MundoBright",
  files_url: "http://localhost/mundobright/files",
  version:"1.0.0a",
  idioma:"es",
  web_url:"http://localhost:4200",
  admin: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
