DROP DATABASE IF EXISTS mundobright;

CREATE DATABASE mundobright;
USE mundobright;

CREATE TABLE `administradores` (
    `id_administrador` INT(11) NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(75) NOT NULL,
    `email` VARCHAR(75) NOT NULL,
    `password` VARCHAR(75) NOT NULL,
    PRIMARY KEY (`id_administrador`)
)ENGINE=InnoDB;

INSERT INTO `administradores` (`nombre`,`email`,`password`) VALUES
    ('Nahuel Sotuyo','nahusotuyo@gmail.com','1234')
;

CREATE TABLE `categorias` (
    `id_categoria` INT(11) NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(75) NOT NULL,
    PRIMARY KEY (`id_categoria`)
)ENGINE=InnoDB;

INSERT INTO `categorias` (`nombre`) VALUES
    ('Juegos y Juguetes'),
    ('Ropa y Calzado para Bebés'),
    ('Lactancia y Alimentación'),
    ('Souvenirs, Cotillón y Fiestas'),
    ('Juegos y Juguetes para Bebés'),
    ('Seguridad para Bebés'),
    ('Cuarto del Bebé')
;

CREATE TABLE `sub_categorias` (
    `id_sub_categoria` INT(11) NOT NULL AUTO_INCREMENT,
    `id_categoria` INT(11) NOT NULL,
    `nombre` VARCHAR(75) NOT NULL,
    PRIMARY KEY (`id_sub_categoria`),
    FOREIGN KEY (`id_categoria`) REFERENCES `categorias`(`id_categoria`)
)ENGINE=InnoDB;

INSERT INTO `sub_categorias` (`id_categoria`,`nombre`) VALUES
    (1,'Juegos de Plaza y Aire Libre'),
    (1,'Vehículos Montables para Niños'),
    (1,'Souvenirs, Cotillón y Fiestas'),
    (2,'Conjuntos'),
    (2,'Bodys'),
    (2,'Calzados'),
    (2,'Vestidos'),
    (3,'Mamaderas y Accesorios'),
    (3,'Leche Maternizada'),
    (3,'Sacaleches'),
    (3,'Baberos para Comer'),
    (4,'Cotillón'),
    (4,'Disfraces'),
    (5,'Mecedoras y Saltarines'),
    (5,'Gimnasios'),
    (5,'Móviles'),
    (5,'Sonajeros'),
    (6,'Mochilas y Porta Bebés'),
    (6,'Pisos de Goma'),
    (6,'Baby Calls'),
    (6,'Puertas de Seguridad'),
    (7,'Cunas, Catres y Moisés'),
    (7,'Ropa de Cuna')
;

CREATE TABLE `productos` (
    `id_producto` INT(11) NOT NULL AUTO_INCREMENT,
    `id_sub_categoria` INT(11) NOT NULL,
    `nombre` VARCHAR(75) NOT NULL,
    `marca` VARCHAR(50) NOT NULL,
    `precio` DECIMAL(8,2) NOT NULL,
    `imagen` VARCHAR(100) NOT NULL,
    `cantidad` INT(5) NOT NULL,
    `fecha_publicacion` DATETIME NOT NULL DEFAULT NOW(),
    PRIMARY KEY (`id_producto`),
    FOREIGN KEY (`id_sub_categoria`) REFERENCES `sub_categorias`(`id_sub_categoria`)
)ENGINE=InnoDB;

INSERT INTO `productos` (`id_sub_categoria`,`nombre`,`marca`,`precio`,`imagen`,`cantidad`) VALUES
    (4,'Conjunto Jogger Shiny Hooded Azul','Adidas',2143,'foto1.png',100),
    (4,'Conjunto Cómodo Ajuar Nenes Varón Pack','Carters',2143,'foto2.png',100),
    (22,'Practicuna Cuna Megababy Completa Mosquitero+organiz+bolso','Creciendo',5189,'foto3.png',100),
    (22,'Cuna 5 En 1: Cuna, Moisés, Colecho, Mesaita Y Escritorio','Musancla',3764.06,'foto4.png',100),
    (18,'Mochila Porta Bebe Carestino Capucha Bolsillo 3 En 1','Carestino',2499,'foto5.png',100)
;

CREATE TABLE `ventas` (
    `id_venta` INT(11) NOT NULL AUTO_INCREMENT,
    `id_preferencia` VARCHAR(200) NOT NULL,
    `metodo_retiro` VARCHAR(30) NOT NULL,
    `fecha` DATETIME NOT NULL DEFAULT NOW(),
    PRIMARY KEY (`id_venta`)
)ENGINE=InnoDB;

CREATE TABLE `productos_x_ventas` (
    `id_venta` INT(11) NOT NULL,
    `id_producto` INT(11) NOT NULL,
    `cantidad` INT(5) NOT NULL,
    `precio_unitario` DECIMAL(8,2) NOT NULL,
    PRIMARY KEY (`id_venta`,`id_producto`),
    FOREIGN KEY (`id_producto`) REFERENCES `productos`(`id_producto`),
    FOREIGN KEY (`id_venta`) REFERENCES `ventas`(`id_venta`)
)ENGINE=InnoDB;

CREATE TABLE `promociones` (
    `id_promocion` INT(11) NOT NULL AUTO_INCREMENT,
    `id_producto` INT(11) NOT NULL,
    `precio_promocion` DECIMAL(8,2) NOT NULL,
    `f1` DATETIME NOT NULL,
    `f2` DATETIME NOT NULL,
    PRIMARY KEY (`id_promocion`),
    FOREIGN KEY (`id_producto`) REFERENCES `productos`(`id_producto`)
)ENGINE=InnoDB;